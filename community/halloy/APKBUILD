# Maintainer: Celeste <cielesti@protonmail.com>
maintainer="Celeste <cielesti@protonmail.com>"
pkgname=halloy
pkgver=2024.13
pkgrel=0
pkgdesc="Rust graphical IRC client supporting IRCv3.2 capabilities"
url="https://github.com/squidowl/halloy"
# loongarch64: fails to build linux-raw-sys & rustix crates
arch="all !loongarch64"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cargo
	cargo-auditable
	openssl-dev
	"
source="https://github.com/squidowl/halloy/archive/$pkgver/halloy-$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	local appid="org.squidowl.halloy"

	install -Dm755 target/release/halloy -t "$pkgdir"/usr/bin

	install -Dm644 assets/linux/$appid.desktop \
		-t "$pkgdir"/usr/share/applications
	install -Dm644 assets/linux/$appid.appdata.xml \
		-t "$pkgdir"/usr/share/metainfo

	local size; for size in 16 24 32 48 64 96 128 256 512; do
		install -Dm644 \
		assets/linux/icons/hicolor/"$size"x"$size"/apps/$appid.png \
		-t "$pkgdir"/usr/share/icons/hicolor/"$size"x"$size"/apps
	done
}

sha512sums="
d482c11e9c6588454291367b3b9a62e7c0b6efe40dfc5899aeb044e3deb476157c0e9b89a90d8a88bcf8b6a0b88b3ab4a877557e6510a7f6ae9a30633e3ce02d  halloy-2024.13.tar.gz
"
